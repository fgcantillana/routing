class CreateDrivers < ActiveRecord::Migration[6.0]
  def change
    create_table :drivers do |t|
      t.string :name
      t.string :phone
      t.string :email
      t.integer :vehicle_id
      t.boolean :vehicle_owner, default: false
      t.integer :stop_id

      t.timestamps
    end
    Driver.create  name:  'driver 1',
                    phone: '555444333',
                    email: 'driver2@routing.cl'
    Driver.create  name:  'driver 2',
                    phone: '555444333',
                    email: 'driver2@routing.cl'
    Driver.create  name:  'driver 3',
                    phone: '555444333',
                    email: 'driver3@routing.cl'
    Driver.create  name:  'driver 4',
                    phone: '555444333',
                    email: 'driver4@routing.cl'
  end
end
