class CreateLoadTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :load_types do |t|
      t.string :name, default: "general"

      t.timestamps
    end
    LoadType.create  name:  'General'
    LoadType.create  name:  'Liquidos'
    LoadType.create  name:  'Refrigerado'
  end
end
