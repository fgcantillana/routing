class CreateVehicles < ActiveRecord::Migration[6.0]
  def change
    create_table :vehicles do |t|
      t.float :capacity
      t.integer :load_type, default: 1
      t.integer :driver_id
      t.timestamps
    end
  end
end
