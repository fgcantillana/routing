class AddFkRoutes < ActiveRecord::Migration[6.0]
  def change
    # FK Routes - Vehicles
    add_foreign_key :routes, :vehicles, column: :vehicle_id
    add_index :routes, :vehicle_id

    # FK ROUTES - Drivers
    add_foreign_key :routes, :drivers, column: :driver_id
    add_index :routes, :driver_id

  end
end
