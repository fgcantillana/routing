class CreateRoutes < ActiveRecord::Migration[6.0]
  def change
    create_table :routes do |t|
      t.datetime :start_at
      t.datetime :ends_at
      t.integer :load_type
      t.integer :load_sum
      t.integer :cities
      t.integer :stops_amount
      t.integer :vehicle_id
      t.integer :driver_id
      t.timestamps
    end
  end
end
