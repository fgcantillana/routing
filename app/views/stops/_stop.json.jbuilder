json.extract! stop, :id, :name, :latitude, :longitude, :created_at, :updated_at
json.url stop_url(stop, format: :json)
