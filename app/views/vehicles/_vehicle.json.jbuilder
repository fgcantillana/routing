json.extract! vehicle, :id, :capacity, :load_type, :driver_id, :created_at, :updated_at
json.url vehicle_url(vehicle, format: :json)
