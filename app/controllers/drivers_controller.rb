class DriversController < ApplicationController
  before_action :set_driver, only: [:show, :edit, :update, :destroy]

  # GET /drivers
  # GET /drivers.json
  def index
    @drivers = Driver.all
  end

  # GET /drivers/1
  # GET /drivers/1.json
  def show
  end

  # GET /drivers/new
  def new
    @driver = Driver.new
    @vehicle = Vehicle.all
    @cities = Stop.all
  end

  # GET /drivers/1/edit
  def edit
      @vehicle = Vehicle.all
      @cities = Stop.all
  end

  # POST /drivers
  # POST /drivers.json
  def create
    @driver = Driver.new(driver_params)
    owner = @driver["vehicle_owner"]
    id_veh = @driver["vehicle_id"]
    vehicle = Vehicle.find_by_id(id_veh)
    if owner == '0'
      @driver['vehicle_id'] = nil
      vehicle.driver_id = nil
    end

    respond_to do |format|
      if @driver.save
        format.html { redirect_to @driver, notice: 'Driver was successfully created.' }
        format.json { render :show, status: :created, location: @driver }
      else
        format.html { render :new }
        format.json { render json: @driver.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      @drivers = (driver_params)
      owner = @drivers["vehicle_owner"]
      id_veh = @drivers["vehicle_id"]
      if !id_veh.empty?
        vehicle = Vehicle.find_by_id(id_veh)
        if owner == '0'
          @drivers['vehicle_id'] = nil
          vehicle.driver_id = nil
        else
          vehicle.driver_id = params['id']
        end
        vehicle.save
      end
      if @driver.update(@drivers)
        format.html { redirect_to @driver, notice: 'Driver was successfully updated.' }
        format.json { render :show, status: :ok, location: @driver }
      else
        format.html { render :edit }
        format.json { render json: @driver.errors, status: :unprocessable_entity }
      end

    end
  end

  # DELETE /drivers/1
  # DELETE /drivers/1.json
  def destroy
    @driver.destroy
    respond_to do |format|
      format.html { redirect_to drivers_url, notice: 'Driver was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_driver
      @driver = Driver.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def driver_params
      params.require(:driver).permit(:name, :phone, :email, :vehicle_id, :vehicle_owner, :stop_id)
    end
end
