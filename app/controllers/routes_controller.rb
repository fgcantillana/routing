class RoutesController < ApplicationController
  before_action :set_route, only: [:show, :edit, :update, :destroy]

  # GET /routes
  # GET /routes.json
  def index
    @routes = Route.all
  end

  # GET /routes/1
  # GET /routes/1.json
  def show
  end

  # GET /routes/new
  def new
    @route = Route.new
    @cities = Stop.all
    @load = LoadType.all
  end

  # GET /routes/1/edit
  def edit
    @cities = Stop.all
    @load = LoadType.all
  end

  # POST /routes
  # POST /routes.json
  def create
    @cities = Stop.all
    @load = LoadType.all
    @route = Route.new(route_params)
    vehiculos = []
    ids_driver = []
    var_id = 0


    # buscar driver con las preferencias anteriores viendo que no tenga rutas asociadas en el dia y no se repita
    carga = if @route['load_type'].nil? then 1 else @route['load_type'] end
    cities = if @route['cities'].nil? then 1 else @route['cities'] end

    vehicles = Vehicle.where(load_type: carga)
    vehicles.each do |ve|
      ruta = Route.where(vehicle_id: ve.id).where('start_at < ? AND ends_at > ?', @route['ends_at'], @route['start_at'])
      if ruta.count == 0
        vehiculos << ve.id
      end
    end
    salida = false
    mensaje = ""
    if !vehiculos.empty?

      # buscar conductor si tiene o no vehiculo
      vehiculos.each do |v|
        veh = Vehicle.find_by_id(v)
        if veh.driver_id.nil?
          #Buscar driver sin vehiculos y ciudad preferida
          drivers = Driver.select(:id).where(vehicle_owner: false, stop_id: @route['cities'])
          if !drivers.nil?
            drivers.each do |d|
              driver = Route.where(driver_id: d.id)
              ids_driver << d.id
              if driver.empty?
                var_id = d.id
                break
              end
            end
            if var_id == 0
              ids_driver.shuffle!
              var_id = ids_driver[0]
            end
            @route.driver_id = var_id
            @route.vehicle_id = veh.id
            salida = true
            break
          else
            drivers = Driver.where(vehicle_owner: false)
            if !driver.nil?
              drivers.each do |d|
                driver = Route.where(driver_id: d.id)
                ids_driver << d.id
                if driver.empty?
                  var_id = d.id
                  break
                end
              end
              if var_id == 0
                ids_driver.shuffle!
                var_id = ids_driver[0]
              end
              @route.driver_id = var_id
              @route.vehicle_id = veh.id
              salida = true
              break
            else
              mensaje = "No encontro conductor"
            end
          end
          puts "****************************************"
          puts @route.driver_id
        else
          puts "Algo #{ veh.id}"
          @route.driver_id = veh.driver_id
          @route.vehicle_id = veh.id
          salida = true
          break
        end
      end
    else
      mensaje = "No existen vehiculos disponibles en ese horario"
    end
    @route.load_type = carga
    @route.cities = cities

    respond_to do |format|
      if salida
          #agregar los datos a la tabla de ruta y registro de rutas
          if @route.save
            format.html { redirect_to @route, notice: 'Route was successfully created.' }
            format.json { render :show, status: :created, location: @route }
          else
            format.html { redirect_to @route, notice: mensaje }
            format.json { render json: @route.errors, status: :unprocessable_entity }
          end
      else
        puts "NO salio"
        format.html { redirect_to @route, notice: mensaje }
        format.json { render json: @route.errors, status: :unprocessable_entity, notice: mensaje }
      end
    end

  end

  # PATCH/PUT /routes/1
  # PATCH/PUT /routes/1.json
  def update
    respond_to do |format|
      if @route.update(route_params)
        format.html { redirect_to @route, notice: 'Route was successfully updated.' }
        format.json { render :show, status: :ok, location: @route }
      else
        format.html { render :edit }
        format.json { render json: @route.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /routes/1
  # DELETE /routes/1.json
  def destroy
    @route.destroy
    respond_to do |format|
      format.html { redirect_to routes_url, notice: 'Route was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_route
      @route = Route.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def route_params
      params.require(:route).permit(:start_at, :ends_at, :load_type, :load_sum, :cities, :stops_amount, :vehicle_id, :driver_id)
    end
end
