class Route < ApplicationRecord
  belongs_to :driver
  has_and_belongs_to_many :stops
end
