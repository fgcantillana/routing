class Driver < ApplicationRecord
  has_many :routes
  has_and_belongs_to_many :vehicles
end
