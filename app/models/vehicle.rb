class Vehicle < ApplicationRecord
  has_many :routes
  has_and_belongs_to_many :drivers
end
