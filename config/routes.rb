Rails.application.routes.draw do
  resources :load_types
  resources :stops
  resources :drivers
  resources :vehicles
  resources :routes
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
